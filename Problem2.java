import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.ZonedDateTime.now;

public class Problem2 {
    /*
    Implement a Java application running on 3 threads.
     The thread names will be NewThread1, NewThread2 and NewThread3.
     The threads will display 6 messages in the console, at an interval of 1 sec.
     The message will be of the form [thread_name] - [current_time], where [thread_name] will be NewThread1, NewThread2 or NewThread3.
     */
    public static void main(String[] args) {
        niceThread t1 = new niceThread("NewThread1");
        niceThread t2 = new niceThread("NewThread2");
        niceThread t3 = new niceThread("NewThread3");
        t1.run();
        t2.run();
        t3.run();
    }
}

class niceThread extends Thread{

    private String name;

    public niceThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        for (int i = 1; i <= 6; i++){
            System.out.println( "threatname "+ this.name + "current_time"+LocalDateTime.now());
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
